<?php 
session_start();
header("Cache-Control: private");
include $_SERVER['DOCUMENT_ROOT'].'/app/core/conf/config.sistema.php'; 
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
	<link rel="shortcut icon" href="<?php echo $servidor?>/vendor/img/hd.svg" />
	<title><?php echo $nombresistema ?></title>

  <!-- CSS  -->
	<link  rel="stylesheet" type="text/css" media="screen,projection" href="<?php echo CSS?>bootstrap.min.css"/>
	<link  rel="stylesheet" type="text/css" media="screen,projection" href="<?php echo CSS?>flexboxgrid.min.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="<?php echo CSS?>jquery-confirm.css">
	<link rel="stylesheet" type="text/css" media="screen,projection" href="<?php echo CSS?>toastr.min.css">
	<link rel="stylesheet" href="<?php echo css?>style.css">
	<script src="<?php echo JS?>jquery-3.4.1.min.js"></script>
<body>
	<header>
		<div class="logo-menu">
			<div class="container">
				<div class="row middle-xs between-xs">
					<div class="logo col-xs-2 col-sm-4 center-xs start-sm "><h2>HD</h2></div>
					<div class="menu col-xs-10 col-sm-8 center-xs end-sm">
						<form id="form_login" class="row middle-xs center-xs end-xs form-inline">
							<div class="input-cont">
								<label>Correo o usuario</label>
								<input type="text" name="email" id="email" placeholder="Email o usuario">
							</div>
							<div class="input-cont">
								<label>Tu contraseña</label>
								<input type="password" class="" name="clave" id="clave" placeholder="clave">
							</div>
							<div class="input-cont">
								<button type="submit" class="btn-ingresar verde" name="btn-login"  onclick="login()" >Entrar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div class="cont-principal container">
		<div class="grid row">
			<div class=" cont-izq col-xs-12 col-md-8 col-sm-6 ">
				<div class="cont-text">
					<h5>Organiza tus contraseñas, cuentas, contactos, para que tengas todo en un solo lugar rapido y facil</h5>
					
				</div>
				<div class="img center-xs	">
					<img src="app/vendor/img/hd.svg" alt="">
				</div>
			</div>
			<div class="col-xs-12 col-md-4 col-sm-6">
				<div class="text-form">
					<h1>Registrate</h1>
					<span>Es muy facil</span>
				</div>
				<form id="form_crear">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" class="form-control" id="user" name="user" placeholder="Usuario">
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" class="form-control" id="pass" name="pass" placeholder="Clave">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<input type="text" class="form-control" id="email" name="email" placeholder="Correo electronico">
							</div>
						</div>
						<hr>
						<div class="col-sm-12">
							<div class="form-group">
								<button type="button" class="verde btn-normal btn-enviar">Registrarse</button>	
							</div>
						</div>
					</div>
		        </form>
		        <div class="row">
		        	<div class="text-form1">
		        		<span>Cree una cuenta facil y rapido, con confianza de sus datos esten resgusrdados.</span>
		        	</div>
		        </div>
			</div>
		</div>
	</div>
	<script src="<?php echo JS?>jquery-confirm.js"></script>
	<script src="<?php echo JS?>toastr.min.js"></script>
	<script src="<?php echo JS?>popper.min.js"></script>
	<script src="<?php echo JS?>bootstrap.min.js"></script>
	<script src="<?php echo JS?>fontawesome.min.js"></script>
	<script src="<?php echo js?>login.js " type="text/javascript" charset="utf-8" async defer></script>
	<script>
		function cerrar(){
			window.close();
		}
	</script>
</body>
</html>