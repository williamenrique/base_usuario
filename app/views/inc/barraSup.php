<div class="box row align-items-center">
	<div class="col-3">
		<div class="box-menu-logo">
			<i class="">icon</i>
			<span>
				<img src="../app/vendor/img/hd.svg" alt="">
			</span>
			<div> creative</div>
		</div>
	</div>
	<div class="col-7">
		<div class="box-buscar">
			<form class="form-buscar">
				<label for="">icon </label>
				<input type="text" name="" value="" placeholder="introduzca su busqueda">
			</form>
		</div>
	</div>
	<div class="col-2">
		<div class="box-user">
			<div class="dropleft cont-sub-menu">
				<a class="" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<img src="<?php echo $_SESSION['imagen_perfil']; ?>" alt="">
				</a>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
					<a onclick="irUser('usuario')" class="dropdown-item">
						<?php echo $_SESSION['nombres']." ".$_SESSION['apellidos']?>
					</a>
					<a class="dropdown-item" href="#">Action</a>
					<a class="dropdown-item" href="#">Another action</a>
					<a class="dropdown-item" href="#" onclick="salir()">Salir del sistema</a>
				</div>
			</div>
			<div class="user-nomb">
				<span><?php echo $_SESSION['user'] ?></span>
			</div>
		</div>
	</div>
</div>
<style>
	
.box{
	border: 1px;
	background: #3367D6;
	height: 50px;
	padding:0px 20px;
	width: 100%;
	margin: 0;
}
.box .box-menu-logo{
	display: flex;
	align-items: center;
}
.box .box-menu-logo i{
	margin: 0px 10px 0px 10px;
	font-size: 25px;
	color: #FFF;
}
.box .box-menu-logo img{
	display: block;
	height:40px;
	width:40px;
	border-radius:200px;
	text-align: center;
	margin-left: 10px;
}
.box .box-buscar{
	position: relative;
}
.box .box-buscar label{
	position: absolute;
	top: 8px;
	left: 10px;
	color :white;
}
.box .box-buscar input{
	padding-left: 60px;
	width: 100%;
	height: 40px;
	border: none;
	border-radius: 6px;
	background: #2850A7;
	color: #FFF;
	font-size: 18px;
}
,
.box .box-buscar .form-buscar ::placeholder{
	color: #FFF;

}
.box .box-user{
	display: flex;
	align-items: center;
}
.box .box-user img{
	display: block;
	height:40px;
	width:40px;
	border-radius:200px;
	text-align: center;
	margin-left: 10px;
}
.box .box-user img{
	-webkit-transition: all .3s ease;
	-o-transition: all .3s ease;
	transition: all .3s ease; 
}
.box .box-user img:hover{
	box-shadow:  0px 0px 10px 2px grey;
}
.box .box-user img:active{
	-webkit-transform: scale(0.95);
	-ms-transform: scale(0.95);
	transform: scale(0.95); 
}

.box .box-user .user-nomb{
	padding: 14px 0px 0px 5px;
	color: #FFF;
}


/*contenido y sidebar*/
.box-main{
	border: 1px solid red;
	display: flex;
}
.box-main .box-sidebar{
	width: 20%;
	border: 1px solid blue;
}
.box-main .box-contenido{
	width: 100%;
	border: 1px solid green;
}
</style>