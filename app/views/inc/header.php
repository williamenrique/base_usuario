<?php 
session_start();
//header("Cache-Control: private");
if(!empty($_SESSION['ingreso'])):
	
include $_SERVER['DOCUMENT_ROOT'].'/app/core/conf/config.sistema.php'; 
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
	<link rel="shortcut icon" href="<?php echo $servidor?>/vendor/img/hd.svg">
	<title><?php echo $nombresistema ?></title>

  <!-- CSS  -->
	<link  rel="stylesheet" type="text/css" media="screen,projection" href="<?php echo CSS?>bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" media="screen,projection" href="<?php echo CSS?>jquery-confirm.css">
	<link rel="stylesheet" type="text/css" media="screen,projection" href="<?php echo CSS?>toastr.min.css">
	<link rel="stylesheet" href="<?php echo css?>style.css">
	<script src="<?php echo JS?>jquery-3.4.1.min.js"></script>
</head>
<body>
	<?php include "barraSup1.php"?>
	
<?php 
else: 
header('Location: ../');
endif;