<div class="box row align-items-center">
	<div class="col-3">
		<div class="box-menu-logo">
			<a href="#">
				<span class="icon-bars icon-menu"></span>
			</a>
			<span><?php echo $nombresistema?></span>
		</div>
	</div>
	<div class="col">
		<div class="box-buscar">
		</div>
	</div>
	<div class="col-2 ">
		<div class="box-user">
			<div class="dropleft cont-sub-menu">
				<a class="" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<img src="<?php echo $_SESSION['imagen_perfil']; ?>" alt="">
				</a>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
					<button type="button" onclick="irA('usuario')" class="dropdown-item">
						<?php echo $_SESSION['nombres']." ".$_SESSION['apellidos']?>
					</button>
					<a class="dropdown-item" href="#">Action</a>
					<a class="dropdown-item" href="#">Another action</a>
					<a class="dropdown-item" href="#" onclick="salir()">Salir del sistema</a>
				</div>
			</div>
			<div class="user-nomb">
				<span><?php echo $_SESSION['user'] ?></span>
			</div>
		</div>
	</div>
</div>