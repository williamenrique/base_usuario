<?php
@session_start();
header('Content-type: text/html; charset=utf-8');
$servidor = "http://hdpersonal.com/app";
$sistema = "app/core/src/";	
$nombresistema = "Agenda Personal";	// nombre del sistema

//constates del sistema
define('JS',$servidor.'/vendor/js/');
define('CSS',$servidor.'/vendor/css/');
define('css',$servidor.'/assets/css/');
define('js',$servidor.'/assets/js/');

define('VIEWS',$_SERVER["DOCUMENT_ROOT"].'/app/views/');
define('ASSETS',$_SERVER["DOCUMENT_ROOT"].'/app/assets/');
define('RAIZ',$_SERVER["DOCUMENT_ROOT"].'/');
define('MODELS',$_SERVER["DOCUMENT_ROOT"].'/'.$sistema.'models/');
define('CONTROLLERS',$_SERVER["DOCUMENT_ROOT"].'/'.$sistema.'controllers/');

date_default_timezone_set('America/Caracas');

define('host' ,'localhost');
define('user' ,  'root');
define('pass' , 'root');
define('db' , 'db_ag_personal');

define('METHOD','AES-256-CBC');
define('SECRET_KEY','$crmLat1n0');
define('SECRET_IV','101712');